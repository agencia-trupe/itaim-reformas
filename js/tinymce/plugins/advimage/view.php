<?php 
require('config.php'); 
function get_images(){
		if ($handle = opendir(IMG_PATH.THUMB_DIR)) {
		    $images = "";
			while (false !== ($file = readdir($handle))) {
				if ($file != "." && $file != "..") {
					$images .= '<a href="#'.IMG_PATH_LIVE.$file.'" rel="image_src">';
					$images .= '<img class="thumb_view" src="'.IMG_PATH_LIVE.THUMB_DIR.$file.'" />';
					$images .= '</a>';
				}
			}
			closedir($handle);
		}
    if(empty($images)){
        return "O Banco de imagens está vazio.";
    }else{
	   return $images;
    }
}
?>

<table style="width:100%">
	<tr>
		<td style="vertical-align:top">
			<div id="library_cont">
				<?php echo get_images(); ?>
			    <div style="clear:both"></div>
			</div>
		</td>
	</tr>
</table>
