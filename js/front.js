$('document').ready( function(){

	$('#slides #animate').cycle({
		pager : $('#navegacao'),
		pause : 1,
		timeout : 6000,
	    pagerAnchorBuilder: function(idx, slide) { 
	        return '<a href="#"></a>';
	    } 		
	});

	$('.servicos-lista h3').click( function(e){
		e.preventDefault();

		var titulo = $(this);
		var sinal  = $(this).find('.sinal');
		var texto  = titulo.next('.detalhe');

		if(texto.hasClass('hid')){
			titulo.addClass('aberto');
			texto.removeClass('hid');
			sinal.html('-');
		}else{
			titulo.removeClass('aberto');
			texto.addClass('hid');
			sinal.html('+');
		}
	});

	$('#form-newsletter').submit( function(e){
		e.preventDefault();

		if($('#newsletter-nome').val() == ''){
			alert('Informe seu nome!');
			return false;
		}

		if($('#newsletter-email').val() == ''){
			alert('Informe seu email!');
			return false;
		}		

		$.post(BASE+'home/cadastro', { nome : $('#newsletter-nome').val(), email : $('#newsletter-email').val() }, function(){
			$('#form-newsletter').addClass('hid');
			setTimeout( function(){
				$('#mensagem-newsletter').removeClass('hid');
				$('#newsletter-nome').val('');
				$('#newsletter-email').val('');	
					setTimeout(function(){
						$('#mensagem-newsletter').addClass('hid');
						setTimeout( function(){
							$('#form-newsletter').removeClass('hid');	
						}, 400);
					}, 10000);	
			}, 400);
		});
	});

	$('#form-orcamento').submit( function(){

		if($('#input-nome').val() == ''){
			alert('Informe seu nome!');
			return false;
		}

		if($('#input-email').val() == ''){
			alert('Informe seu email!');
			return false;
		}

		if($('#input-cidade').val() == ''){
			alert('Informe sua cidade!');
			return false;
		}

		if($('#input-estado').val() == ''){
			alert('Informe seu estado!');
			return false;
		}

	});

	if($('#envio-ok').length){
		setTimeout( function(){
			$('#envio-ok').fadeOut('slow', function(){
				$(this).remove();
			});
		}, 10000);
	}

});