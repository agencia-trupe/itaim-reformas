<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Servicos extends MY_Frontcontroller {

	function __construct(){
		parent::__construct();
	}

	function index(){
		$this->load->view('servicos');
	}

	function detalhes($slug_servico = false){

		$servicos = array(
			'alvenaria',
			'acabamento',
			'eletrica',
			'gesso',
			'hidraulica',
			'pintura'
		);

		if(!$slug_servico || !in_array($slug_servico, $servicos))
			redirect('servicos/index');


		$textos['alvenaria'] = array(
			'titulo' => 'ALVENARIA',
			'imagem' => 'alvenaria.jpg',
			'thumb'  => 'alvenaria-thumb.jpg',
			'texto'  => <<<STR
<ul>
<li>Reformas e reparos em geral;</li>
<li>Demolição e construção; </li>
<li>Modernização de banheiros e cozinhas;</li>
<li>Infraestrutura para instalação de ar-condicionado, com instalação de drenos e tubulações.</li>
</ul>
STR
			,'servicos' => array()
		);

		$textos['acabamento'] = array(
			'titulo' => 'ACABAMENTO',
			'imagem' => 'acabamento.jpg',
			'thumb'  => 'acabamento-thumb.jpg',
			'texto'  => <<<STR
<ul>
<li>Assentamento de pisos e revestimentos, porcelanatos, pisos cerâmicos, mármores, granitos, azulejos, pastilhas de vidro e porcelana;</li>
<li>Raspagem de tacos e aplicação de resinas;</li>
<li>Aplicação de cimento queimado.</li>
</ul>
STR
			,'servicos' => array(
				//array( 
				//	'titulo' => 'Serviço oferecido lorem ipsum dolo sit amet',
				//	'texto'  => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'
				//),				
			)
		);

		$textos['eletrica'] = array(
			'titulo' => 'ELÉTRICA',
			'imagem' => 'eletrica.jpg',
			'thumb'  => 'eletrica-thumb.jpg',
			'texto'  => <<<STR
<ul>
<li>Instalações residenciais e comerciais, de baixa e alta tensão;</li>
<li>Padrões monofásico e trifásico;</li>
<li>Reparos elétricos em geral, quadros de luz, fios, cabos, interruptores, tomadas; Instalação e mudanças de tomadas e interruptores;</li>
<li>Rede lógica e elétrica.</li>
</ul>
STR
			,'servicos' => array()			
		);

		$textos['gesso'] = array(
			'titulo' => 'GESSO',
			'imagem' => 'gesso.jpg',
			'thumb'  => 'gesso-thumb.jpg',
			'texto'  => <<<STR
<ul>
<li>Drywall:  paredes, forros, divisórias, estantes, painéis, cortineiros;</li>
<li>Sancas, molduras e nichos. </li>
</ul>
STR
			,'servicos' => array()
		);

		$textos['hidraulica'] = array(
			'titulo' => 'HIDRÁULICA',
			'imagem' => 'hidraulica.jpg',
			'thumb'  => 'hidraulica-thumb.jpg',
			'texto'  => <<<STR
<ul>
<li>Instalação e substituição de louças e metais;</li>
<li>Criação e isolamento de pontos de água e esgoto;</li>
<li>Instalação e reparo de registros (gaveta e pressão);</li>
<li>Instalação e reparo de válvulas Hydra;</li>
<li>Localização e eliminação de vazamentos.</li>
</ul>
STR
			,'servicos' => array()
		);

		$textos['pintura'] = array(
			'titulo' => 'PINTURA',
			'imagem' => 'pintura.jpg',
			'thumb'  => 'pintura-thumb.jpg',
			'texto'  => <<<STR
<ul>
<li>Pinturas residenciais e comerciais (interna e externa); </li>
<li>Selagem de fissuras e trincas superficiais; </li>
<li>Pintura de portas e janelas;</li>
<li>Impermeabilizações; </li>
<li>Restaurações em geral.</li>
</ul>
STR
			,'servicos' => array()
		);

		$data['texto'] = $textos[$slug_servico];
		$data['marcar'] = $slug_servico;

		$this->headervar['og'] = array(
			'title' => "Itaim Reformas - " . ucfirst(mb_strtolower($data['texto']['titulo'])),
			'image' => base_url('_imgs/servicos/'.$data['texto']['imagem']),
			'description' => strip_tags($data['texto']['texto'])
		);

		$this->load->view('servicos_detalhes', $data);
	}

}
