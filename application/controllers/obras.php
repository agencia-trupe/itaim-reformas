<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Obras extends MY_Frontcontroller {

	function __construct(){
		parent::__construct();
	}

	function index(){
		$data['obras'] = $this->db->order_by('ordem', 'asc')->get('obras')->result();
		$this->load->view('obras', $data);
	}

	function detalhes($slug_obra = false){

		if(!$slug_obra)
			redirect('obras/index');

		$query = $this->db->get_where('obras', array('slug' => $slug_obra))->result();

		if (!isset($query[0]))
			redirect('obras/index');
		else
			$data['obra'] = $query[0];

		$data['obra']->imagens = $this->db->get_where('obras_imagens', array('obras_id' => $data['obra']->id))->result();

		$this->load->view('obras_detalhes', $data);
	}
}
