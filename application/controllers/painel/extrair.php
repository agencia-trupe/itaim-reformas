<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Extrair extends MY_Admincontroller {

    var $unidade,
        $titulo;

    function __construct(){
   		parent::__construct();
    }

    function index(){
    	$data['cadastros'] = $this->db->get('newsletter')->result();
        $data['num_cadastros'] = $this->db->get('newsletter')->num_rows();
    	$this->load->view('painel/extrair', $data);
    }

    function excluir($id){

        $exclude = $this->db->where('id', $id)->delete('newsletter');

        if($exclude){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', $this->unidade.' excluido com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao excluir '.$this->unidade);
        }

        redirect('painel/'.$this->router->class.'/index', 'refresh');
    }    
}