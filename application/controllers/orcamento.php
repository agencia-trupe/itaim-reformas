<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Orcamento extends MY_Frontcontroller {

	function __construct(){
		parent::__construct();
	}

	function index(){
		$this->load->view('orcamento');
	}

	function enviarOrcamento(){

        $emailconf['charset'] = 'utf-8';
        $emailconf['mailtype'] = 'html';
        
        /*
        $emailconf['protocol'] = 'smtp';        
        $emailconf['smtp_host'] = 'smtp.trupe.com.br';
        $emailconf['smtp_user'] = 'noreply@trupe.com.br';
        $emailconf['smtp_pass'] = 'moura-noreply1';
        $emailconf['smtp_port'] = 587;
        */
        $emailconf['crlf'] = "\r\n";
        $emailconf['newline'] = "\r\n";        
        
        $this->load->library('email');

        $this->email->initialize($emailconf);

        $u_nome = $this->input->post('nome');
        $u_email = $this->input->post('email');
        $u_tel = $this->input->post('telefone');
        $u_end = $this->input->post('endereco');
        $u_cid = $this->input->post('cidade');
        $u_est = $this->input->post('estado');
        $u_desc = $this->input->post('descricao');

        $from = 'noreply@itaimreformas.com.br';
        $fromname = 'Itaim Reformas - Orçamento';
        $to = 'contato@itaimreformas.com.br';
        $bc = FALSE;
        $bcc = 'bruno@trupe.net';

        $assunto = 'Orçamento';
        $email = "<html>
                    <head>
                        <style type='text/css'>
                            .tit{
                                font-weight:bold;
                                font-size:14px;
                                color:#5DB8C9;
                                font-family:Arial;
                            }
                            .val{
                                color:#000;
                                font-size:12px;
                                font-family:Arial;
                            }
                        </style>
                    </head>
                    <body>
                    <span class='tit'>Nome :</span> <span class='val'>$u_nome</span><br />
                    <span class='tit'>E-mail :</span> <span class='val'>$u_email</span><br />
                    <span class='tit'>Telefone :</span> <span class='val'>$u_tel</span><br />
                    <span class='tit'>Endereço :</span> <span class='val'>$u_end</span><br />
                    <span class='tit'>Cidade :</span> <span class='val'>$u_cid</span><br />
                    <span class='tit'>Estado :</span> <span class='val'>$u_est</span><br />
                    <span class='tit'>Descrição :</span> <span class='val'>$u_desc</span><br />
                    </body>
                    </html>";

        $this->email->from($from, $fromname);
        $this->email->to($to);
        
        if($bc)
            $this->email->bc($bc);
        if($bcc)
            $this->email->bcc($bcc);
        
        $this->email->reply_to($u_email);

        $this->email->subject($assunto);
        $this->email->message($email);

        $this->session->set_flashdata('envio_status', TRUE);

        $this->email->send();		

		redirect('orcamento/index', 'refresh');
	}
}
