<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();
    }

    function index(){
   		$this->load->view('home');
    }

    function cadastro(){
    	$nome = $this->input->post('nome');
    	$email = $this->input->post('email');

    	if($this->db->get_where('newsletter', array('email' => $email))->num_rows() == 0 && $this->checarEmail($email)){
	    	$this->db->set('nome', $nome)
	    			 ->set('email', $email)
	    			 ->insert('newsletter');
		}    	
    }

    function checarEmail($email = false){
        return preg_match("/^[_a-z0-9-]+(\.[_a-z0-9+-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i", $email);
    }

}