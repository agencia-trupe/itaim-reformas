
<div class="titulo-internas">

	<div class="centro">

		<h1>CONTATO</h1>

		<h2><!-- Lorem ipsum dolor sit amet consectetur.--></h2>

	</div>

</div>

<div class="container centro contato">

	<div id="mapa">
		<iframe width="958" height="250" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com.br/maps?q=Rua+Tabapu%C3%A3,+145+-+1%C2%BA+andar&amp;ie=UTF8&amp;hq=&amp;hnear=R.+Tabapu%C3%A3,+145+-+Itaim+Bibi,+S%C3%A3o+Paulo,+04533-010&amp;gl=br&amp;ll=-23.582187,-46.672055&amp;spn=0.01353,0.024762&amp;t=m&amp;z=14&amp;output=embed"></iframe>		
	</div>

	<div class="coluna contato">
		<h3>CONTATO</h3>
		<div class="telefone">(11) 3583-0074</div>
		<div class="endereco">
			Rua Tabapuã, 145 - 1&ordm; andar<br>
			Itaim Bibi - São Paulo/SP
		</div>
	</div>

	<div class="coluna form">
		<h3>ENVIE-NOS UMA MENSAGEM</h3>

		<?php if ($this->session->flashdata('envio_status')): ?>
			<div id="envio-ok">
				Mensagem enviada! Responderemos assim que possível!
			</div>
		<?php endif ?>

		<form id="form-contato" method="post" action="contato/enviar">

			<div class="coluna-form-esq">
				<input type="text" name="nome" id="contato-nome" placeholder="Nome" required>
				<input type="email" name="email" id="contato-email" placeholder="E-mail" required>
				<input type="text" name="telefone" id="contato-telefone" placeholder="Telefone">
			</div>

			<div class="coluna-form-dir">
				<textarea name="mensagem" id="contato-mensagem" placeholder="Mensagem" required></textarea>
			</div>

			<div id="contato-submit">
				<input type="submit" value="ENVIAR">
			</div>

		</form>

	</div>

</div>