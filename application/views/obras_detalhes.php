<div class="titulo-internas">

	<div class="centro">

		<h1>OBRAS</h1>

		<!-- <h2>Lorem ipsum dolor sit amet consectetur.</h2> -->

	</div>

</div>

<div class="container centro obrasd">

	<div id="detalhe-obra">

		<h3><?=$obra->titulo?></h3>

		<h4><?=$obra->titulo2?></h4>

		<?=$obra->texto?>

	</div>

	<div class="coluna esquerda">

		<?php $paginar = FALSE;?>

		<div id="animate-ul">

			<ul class="lista-thumbs">

				<?php if ($obra->imagens): ?>
					<?php foreach ($obra->imagens as $key => $value): ?>

						<li>
							<a href="_imgs/obras/<?=$value->imagem?>">
								<img src="_imgs/obras/thumbs/<?=$value->imagem?>">
							</a>
						</li>

						<?php if (($key + 1)%8 == 0 && $key > 0 ): ?>
							</ul><ul class="lista-thumbs" style='display:none;'>
							<?$paginar = true?>
						<?php endif ?>

					<?php endforeach ?>	
				<?php endif ?>

			</ul>

		</div>

		<?php if ($paginar): ?>
			<div class="paginacao-thumbs">
				<a href="#" title="Página Anterior" id="pagina-anterior">&laquo;</a>
				<a href="#" title="Próxima Página" id="proxima-pagina">&raquo;</a>
			</div>
		<?php endif ?>

	</div>	

	<div class="coluna direita">
		<div id="imagem-ampliada">
			<img src="_imgs/obras/<?=$obra->imagens[0]->imagem?>">
		</div>
	</div>

</div>

<?php if ($paginar): ?>
	<script defer>
	$('document').ready( function(){

		$('#animate-ul').cycle({
			timeout : 0,
			prev : $('#pagina-anterior'),
			next : $('#proxima-pagina')
		});

	});
	</script>
<?php endif ?>