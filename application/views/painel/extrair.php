<h1>Cadastros</h1>

<?php if ($num_cadastros > 0): ?>
	<div id="submenu">
		<a href="index.php/painel/download" class="lista">Baixar em formato CSV</a>
	</div>
<?php endif ?>

<?php if ($cadastros): ?>

	<table>

		<thead>
			<tr>
				<th>Nome</th>
				<th>E-mail</th>
				<th class="option-cell"></th>
			</tr>
		</thead>

		<tbody>
			<?php foreach ($cadastros as $key => $value): ?>
				
				<tr>
					<td><?=$value->nome?></td>
					<td><?=$value->email?></td>
					<td><a class="delete" href="<?=base_url('index.php/painel/'.$this->router->class.'/excluir/'.$value->id)?>">Excluir</a></td>
				</tr>

			<?php endforeach ?>
		</tbody>

	</table>
	
<?php else: ?>

	<h2 style="margin:30px 0;">Nenhum cadastro</h2>
	
<?php endif ?>