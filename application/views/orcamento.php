<div class="titulo-internas">

	<div class="centro">

		<h1>ORÇAMENTO</h1>

		<!-- <h2>Lorem ipsum dolor sit amet consectetur.</h2> -->

	</div>

</div>

<div class="container centro">

	<?php if ($this->session->flashdata('envio_status')): ?>
		<div id="envio-ok">
			Orçamento enviado com sucesso! Responderemos assim que possível.
		</div>
	<?php endif ?>

	<form method="post" action="orcamento/enviarOrcamento" id="form-orcamento">

		<div class="coluna-form esquerda">

			<input type="text" name="nome" id="input-nome" placeholder="Nome" required>

			<input type="email" name="email" id="input-email" placeholder="E-mail" required>

			<input type="text" name="telefone" id="input-telefone" placeholder="Telefone">

			<input type="text" name="endereco" id="input-endereco" placeholder="Endereço">

			<input type="text" name="cidade" id="input-cidade" placeholder="Cidade" class="inline-large" required>

			<input type="text" name="estado" id="input-estado" placeholder="Estado" class="inline-small" required maxlength="2">

		</div>

		<div class="coluna-form direita">

			<textarea name="descricao" id="input-descricao" placeholder="Descrição dos serviços"></textarea>

		</div>

		<div class="submit-placeholder">

			<input type="submit" value="ENVIAR">

		</div>

	</form>

</div>
