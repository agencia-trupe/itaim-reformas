<div class="titulo-internas">

	<div class="centro">
		<h1>OBRAS</h1>
		<!-- <h2>Lorem ipsum dolor sit amet consectetur.</h2> -->
	</div>

</div>

<div class="container centro">

	<?php foreach ($obras as $key => $value): ?>
		<a href="obras/detalhes/<?=$value->slug?>" class="link-obras<?if(($key+1)%4 == 0 && $key > 0)echo " semmargem"?>">
			<img src="_imgs/obras/<?=$value->thumb?>" alt="<?=$value->titulo.' '.$value->titulo?>">
			<h2><?=$value->titulo?></h2>
			<h3><?=$value->titulo2?></h3>
			<p>
				<?=$value->descricao?>
			</p>
		</a>		
	<?php endforeach ?>

</div>
