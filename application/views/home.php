<div id="slides">

	<div class="centro">

		<div id="animate">

			<div class="slide slide1">

				<h1>PROFISSIONAIS QUALIFICADOS</h1>

				<h2>A Itaim Reformas conta com profissionais experientes para execução de projetos de reformas completas.</h2>

				<a href="orcamento" title="Solicite um orçamento!">Solicite um orçamento!</a>

			</div>

			<div class="slide slide2">

				<h1>REFORMAS</h1>

				<h2>Vai reformar? Conte com a Itaim Reformas. Realizamos desde o projeto até a execução completa da obra.</h2>

				<a href="orcamento" title="Solicite um orçamento!">Solicite um orçamento!</a>

			</div>	

			<div class="slide slide3">

				<h1>SERVIÇOS</h1>

				<h2>Prestamos todos os serviços em alvenaria, elétrica, hidráulica, acabamento, gesso e pintura.</h2>

				<a href="servicos" style="width:196px;" title="Conheça nossos serviços!">Conheça nossos serviços!</a>

			</div>	

		</div>

	</div>

</div>

<div id="navegacao"></div>

<div id="chamadas-container" class="centro">

	<a href="servicos" title="Nossos Serviços" class="chamada servicos">
		<h3>NOSSOS SERVIÇOS</h3>
		<p>
			A Itaim Reformas está apta para atender com qualidade, pontualidade e profissionalismo.
		</p>
		<div class="saiba-mais">
			<span>saiba mais &raquo;</span>
		</div>
	</a>

	<a href="orcamento" title="Orçamento Online" class="chamada orcamento">
		<h3>ORÇAMENTO ONLINE</h3>
		<p>
			Solicite um orçamento agora mesmo e venha ser um cliente Itaim Reformas.
		</p>
		<div class="saiba-mais">
			<span>saiba mais &raquo;</span>
		</div>
	</a>

	<a href="http://www.imoveisnoitaim.com.br/" target="_blank" title="Imóveis no Itaim" class="chamada imoveis">
		<h3>IMÓVEIS NO ITAIM</h3>
		<p>
			Conheça nossa imobiliária e as opções disponíveis para venda e locação na região do Itaim Bibi.
		</p>
		<div class="saiba-mais">
			<span>saiba mais &raquo;</span>
		</div>
	</a>

</div>