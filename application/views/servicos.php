<div class="titulo-internas">

	<div class="centro">

		<h1>SERVIÇOS</h1>

		<h2><!-- Lorem ipsum dolor sit amet consectetur.--></h2>

	</div>

</div>

<div class="container centro">

	<a href="servicos/detalhes/alvenaria" title="Alvenaria" class="link-servicos alvenaria">

		<h2>ALVENARIA</h2>

		<div class="olho">
			Realizamos todos os serviços de alvenaria, reparos e reformas em paredes, teto, pisos, telhados, paredes de tijolos, concreto ou gesso.
		</div>

	</a>

	<a href="servicos/detalhes/acabamento" title="Acabamento" class="link-servicos acabamento">

		<h2>ACABAMENTO</h2>

		<div class="olho">
			Executamos serviços com qualidade, e profissionais capacitados. Desde assentamento de pisos a aplicação de cimento queimado.
		</div>

	</a>

	<a href="servicos/detalhes/eletrica" title="Elétrica" class="link-servicos eletrica semmargem">

		<h2>ELÉTRICA</h2>

		<div class="olho">
			Prestamos serviços de instalação e reparos elétricos. Realizamos inspeções prévias, e fazemos um relatório completo das necessidades.
		</div>

	</a>

	<a href="servicos/detalhes/gesso" title="Gesso" class="link-servicos gesso">

		<h2>GESSO</h2>

		<div class="olho">
			Projetos com qualidade, desde forros a colocação de sancas, solicite um orçamento agora.
		</div>

	</a>

	<a href="servicos/detalhes/hidraulica" title="Hidráulica" class="link-servicos hidraulica">

		<h2>HIDRÁULICA</h2>

		<div class="olho">
			Temos uma equipe técnica qualificada, seja para um pequeno reparo ou grandes instalações.
		</div>

	</a>

	<a href="servicos/detalhes/pintura" title="Pintura" class="link-servicos pintura semmargem">

		<h2>PINTURA</h2>

		<div class="olho">
			Realizamos todos os serviços de pintura, desde fachadas a ambientes inteiros, com qualidade, forração de pisos e proteção dos móveis, confira.
		</div>

	</a>

</div>