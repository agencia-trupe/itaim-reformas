<div class="titulo-internas">

	<div class="centro">

		<h1>EMPRESA</h1>

		<!-- <h2>Lorem ipsum dolor sit amet consectetur.</h2> -->

	</div>

</div>

<div class="container centro">

	<div class="coluna esquerda">
		<img src="_imgs/empresa/empresa.jpg" alt="Itaim Reformas">
	</div>

	<div class="coluna direita">
		<p>
			A Itaim Reformas & Manutenção foi criada para atender primeiramente as necessidades dos clientes de nossa imobiliária, INI - Imóveis no Itaim, que buscavam profissionais para reformar imóveis recém-comprados ou alugados.
 		</p>

 		<p>
			Nossa empresa é formada por profissionais especializados em cada área de atuação, o que possibilita oferecer soluções adequadas para cada necessidade.
		</p>

		<p class="destaque">
			Atendemos clientes residenciais, corporativos e arquitetos. Os serviços por nós executados são garantidos por 03 meses após sua conclusão através de contrato e Nota Fiscal.
		</p>

		<p>
			Por questões logísticas, limitamos nossa área de atuação aos seguintes bairros: Itaim Bibi, Vila Olímpia, Vila Nova Conceição, Jardins, Moema, Brooklin e Campo Belo.
		</p>

	</div>

</div>