<div class="titulo-internas">

	<div class="centro">

		<h1>SERVIÇOS</h1>

		<h2><!-- Lorem ipsum dolor sit amet consectetur.--></h2>

	</div>

</div>

<div class="container centro">

	<div class="coluna esquerda">

		<ul class="submenu-servicos esquerda">
			<li><a href="servicos/detalhes/alvenaria" title="Alvenaria" <?if($marcar=='alvenaria')echo"class='ativo'"?>>ALVENARIA</a></li>
			<li><a href="servicos/detalhes/acabamento" title="Acabamento" <?if($marcar=='acabamento')echo"class='ativo'"?>>ACABAMENTO</a></li>
			<li><a href="servicos/detalhes/eletrica" title="Elétrica" <?if($marcar=='eletrica')echo"class='ativo'"?>>ELÉTRICA</a></li>
		</ul>
		<ul class="submenu-servicos">
			<li><a href="servicos/detalhes/gesso" title="Gesso" <?if($marcar=='gesso')echo"class='ativo'"?>>GESSO</a></li>
			<li><a href="servicos/detalhes/hidraulica" title="Hidráulica" <?if($marcar=='hidraulica')echo"class='ativo'"?>>HIDRÁULICA</a></li>
			<li><a href="servicos/detalhes/pintura" title="Pintura" <?if($marcar=='pintura')echo"class='ativo'"?>>PINTURA</a></li>
		</ul>

	</div>

	<div class="coluna direita">

		<div class="servicos-imagem">
			<img src="_imgs/servicos/<?=$texto['imagem']?>">
		</div>

		<div class="servicos-texto">
			<h2><?=$texto['titulo']?></h2>
			<?=$texto['texto']?>

			<div class="orcamento">

				<a href="orcamento" class="action" title="Solicite um orçamento!">Solicite um orçamento!</a>

				<div class="facebook-like"><div class="fb-like" data-send="false" data-href="<?=current_url()?>" data-layout="button_count" data-font="lucida grande" data-width="140" data-show-faces="false"></div></div>
				<div class="twitter-twet"><a href="https://twitter.com/share" class="twitter-share-button" data-text="Confira os serviços da Itaim Reformas" data-lang="pt">Tweetar</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script></div>

			</div>
		</div>

		<div class="servicos-lista">
			<?php foreach ($texto['servicos'] as $value): ?>
				<h3>
					<div class="sinal">+</div>
					<?=$value['titulo']?>
				</h3>
				<div class="detalhe hid">
					<?=$value['texto']?>
				</div>
			<?php endforeach ?>
		</div>		

	</div>

</div>