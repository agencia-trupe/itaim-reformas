<header>

	<div class="centro">

		<nav class="esquerda">
			<ul>
				<li><a href="home" title="Página Inicial" id="mn-home" <?if($this->router->class=='home')echo" class='ativo'"?>>Home</a></li>
				<li><a href="empresa" title="Nossa Empresa" id="mn-empresa" <?if($this->router->class=='empresa')echo" class='ativo'"?>>Empresa</a></li>
				<li><a href="servicos" title="Nossos Serviços" id="mn-servicos" <?if($this->router->class=='servicos')echo" class='ativo'"?>>Serviços</a></li>
			</ul>		
		</nav>

		<a href="home" title="Página Inicial" id="link-home">
			<img src="_imgs/layout/logo.png" alt="Itaim Reformas">
		</a>

		<nav class="direita">
			<ul>
				<li><a href="orcamento" title="Orçamento Online" id="mn-orcamento" <?if($this->router->class=='orcamento')echo" class='ativo'"?>>Orçamento</a></li>
				<li><a href="obras" title="Obras" id="mn-obras" <?if($this->router->class=='obras')echo" class='ativo'"?>>Obras</a></li>
				<li><a href="contato" title="Entre em Contato" id="mn-contato" <?if($this->router->class=='contato')echo" class='ativo'"?>>Contato</a></li>
			</ul>		
		</nav>

	</div>
	
</header>

<div class="main main-<?=$this->router->class?> main-<?=$this->router->class?>-<?=$this->router->method?>">