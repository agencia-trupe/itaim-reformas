<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="pt-BR"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="pt-BR"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="pt-BR"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="pt-BR"> <!--<![endif]-->
<head>
  <meta charset="utf-8">

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>Itaim Reformas</title>
  <meta name="description" content="Itaim Reformas - Reformas no Itaim, Vila Olimpia, Vila Nova conceição e zona sul. Somos especialistas na região. Confira!">
  <meta name="keywords" content="reformas,construção,reformas no itaim,reformas zona sul,reformas vila nova conceição,reforma de apartamentos,especialistas em reformas,pintura,alvenaria,imoveis no itaim bibi,gesso,reparos,eletricista,hidráulica,elétrica,manutenção,reformas vila olimpia" />
  <meta name="robots" content="index, follow" />
  <meta name="author" content="Trupe Design" />
  <meta name="copyright" content="2013 Trupe Design" />

  <meta name="viewport" content="width=960px,initial-scale=1">

  <meta property="og:site_name" content="Itaim Reformas"/>
  <meta property="og:type" content="website"/>
  <meta property="og:url" content="<?=current_url()?>"/>
  <meta property="og:title" content="<?echo (isset($og) && isset($og['title']) && $og['title']) ? $og['title'] : "Itaim Reformas"?>"/>
  <meta property="og:image" content="<?echo (isset($og) && isset($og['image']) && $og['image']) ? $og['image'] : base_url('_imgs/layout/fb.png')?>"/>
  <meta property="og:description" content="<?echo (isset($og) && isset($og['description']) && $og['description']) ? $og['description'] : "Itaim Reformas - Reformas no Itaim, Vila Olimpia, Vila Nova conceição e zona sul. Somos especialistas na região. Confira!"?>"/>

  <meta property="fb:admins" content="100002297057504" />
  
  <base href="<?= base_url() ?>">
  <script> var BASE = '<?= base_url() ?>'</script>

  <?CSS(array('reset', 'base', 'fontface/stylesheet', 'fancybox/fancybox', $this->router->class, $load_css))?>  
  

  <?if(ENVIRONMENT == 'development'):?>
    
    <?JS(array('modernizr-2.0.6.min', 'less-1.3.0.min', 'jquery-1.8.0.min', $this->router->class, $load_js))?>
    
  <?else:?>

    <?JS(array('modernizr-2.0.6.min', $this->router->class, $load_js))?>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-1.8.0.min.js"><\/script>')</script>

  <?endif;?>

</head>
<body>
  <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>