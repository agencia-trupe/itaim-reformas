
  </div> <!-- fim da div main -->

  <footer>

    <div class="newsletter">
      <div class="centro">

        <div class="texto-newsletter">
          CADASTRE-SE EM NOSSA NEWSLETTER PARA RECEBER NOVIDADES E INFORMATIVOS:
        </div>

        <div id="mensagem-newsletter" class="hid">
          Cadastro efetuado com sucesso!
        </div>        

        <form method="post" action="home/cadastro" id="form-newsletter">

          <input type="text" name="newsletter-nome" id="newsletter-nome" placeholder="Nome">
          <input type="email" name="newsletter-email" id="newsletter-email" placeholder="E-mail">
          <input type="submit" value="ENVIAR">

        </form>

      </div>
    </div>

    <div class="info">
      <div class="centro">

        <div class="links">
          <h3>LINKS</h3>

          <ul>
            <li>&raquo; <a href="home" title="Página Inicial">Home</a></li>
            <li>&raquo; <a href="empresa" title="Nossa Empresa">Empresa</a></li>
            <li>&raquo; <a href="servicos" title="Nossos Serviços">Serviços</a></li>
          </ul>
          <ul>
            <li>&raquo; <a href="orcamento" title="Orçamento Online">Orçamento</a></li>
            <li>&raquo; <a href="obras" title="Obras">Obras</a></li>
            <li>&raquo; <a href="contato" title="Entre em Contato">Contato</a></li>
          </ul>

        </div>

        <div class="contato">
          <h3>CONTATO</h3>
          <div class="container">
            <div class="telefone">(11) 3583-0074</div>
            <div class="endereco">
              Rua Tabapuã, 145 <br> 1&ordm; andar<br>
              Itaim Bibi - São Paulo/SP
            </div>
            <div class="links-social" style="display:none;">
              <a href="" title="Nossa Página no Facebook"><img src="_imgs/layout/facebook.png"></a>
              <a href="" title="Nosso Twitter"><img src="_imgs/layout/twitter.png"></a>
            </div>
          </div>
        </div>

      </div>
    </div>

    <div class="assinatura">
      <div class="centro">
        &copy; <?=Date('Y')?> Itaim Reformas &bull; Todos os direitos reservados &bull; <a href="http://www.trupe.net" title="Criação de Sites"><span>Criação de Sites: Trupe Agência Criativa</span><img src="_imgs/layout/trupe.png" alt="Trupe Agência Criativa"></a>
      </div>
    </div>
  
  </footer>
  
  
  <?if(ENVIRONMENT != 'development' && GOOGLE_ANALYTICS != FALSE):?>
    <script>
      window._gaq = [['_setAccount','UA<?=GOOGLE_ANALYTICS?>'],['_trackPageview'],['_trackPageLoadTime']];
      Modernizr.load({
        load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
      });
    </script>
  <?endif;?>

  <?JS(array('cycle', 'fancybox', 'front'))?>
  
</body>
</html>